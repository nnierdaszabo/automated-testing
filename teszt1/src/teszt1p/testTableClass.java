package teszt1p;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class testTableClass {
	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("http://toolsqa.com/selenium-webdriver/handle-dynamic-webtables-in-selenium-webdriver/");
		String stableValueSearchingFor = "Licensing";
		for (int i = 1; i <= 3; i++) {
			String columnText = driver.findElement(By.xpath(".//*[@id='post-2924']/div/div/div/div/div/div/table/tbody/tr[1]/th["+i+"]")).getText();
			if (columnText.equalsIgnoreCase(stableValueSearchingFor)) {
				for ( int j = 2; j <= 3; j++ ) {
					String rowText = driver.findElement(By.xpath(".//*[@id='post-2924']/div/div/div/div/div/div/table/tbody/tr["+j+"]/td["+i+"]")).getText();
					if (rowText.equalsIgnoreCase("Paid")) {
						System.out.println("Column number is " + i);
						System.out.println("Row number is " + j);
						break;
					}
				}
			}
		}
		driver.quit();
	}
}
