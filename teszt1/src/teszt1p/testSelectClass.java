package teszt1p;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class testSelectClass {
	
	public static void main(String[] args) {
		/* http://toolsqa.com/selenium-webdriver/dropdown-multiple-select-operations/ */
		WebDriver driver = new FirefoxDriver();
		driver.get("http://toolsqa.com/automation-practice-form/");
		//Select oSelect = new Select(driver.findElement(By.id("continents")));
		//oSelect.selectByIndex(4); //select by index		
		//oSelect.selectByVisibleText("Europe"); //select by visible text		
		//oSelect.deselectByIndex(2); // deselect by
		
		/* Boolean selectIsMultiple = oSelect.isMultiple();
		System.out.println(selectIsMultiple); */
		
		/* type select option texts on console */
		/* List<WebElement> selectCount = oSelect.getOptions();
		int selectSize = selectCount.size();
		for (int i = 0; i < selectSize; i++) {
			String selectValue = selectCount.get(i).getText();
			System.out.println(selectValue);
		} */
		
		/* Practice Exercise - 1 */
		/* oSelect.selectByIndex(2);
		oSelect.selectByVisibleText("Africa");
		List<WebElement> selectCount = oSelect.getOptions();
		int selectSize = selectCount.size();
		for (int i = 0; i < selectSize; i++) {
			String selectValue = selectCount.get(i).getText();
			System.out.println(selectValue);
		}
		driver.quit(); */
		
		/* Practice Exercise -2 */
		Select oSelect = new Select(driver.findElement(By.id("selenium_commands")));
		oSelect.selectByIndex(0);
		oSelect.deselectByIndex(0);
		oSelect.selectByVisibleText("Navigation Commands");
		oSelect.deselectByVisibleText("Navigation Commands");
		List<WebElement> selectOptionsNumber = oSelect.getOptions();
		int iSelectSize = selectOptionsNumber.size();
		for (int i = 0; i < iSelectSize; i++) {
			String sSelectOptionName = oSelect.getOptions().get(i).getText();
			System.out.println(sSelectOptionName);
			oSelect.selectByIndex(i);
		}
		oSelect.deselectAll();
	}	
}
