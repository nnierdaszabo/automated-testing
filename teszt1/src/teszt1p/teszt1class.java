package teszt1p;
	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.firefox.FirefoxDriver;

public class teszt1class {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		
		/* http://toolsqa.com/selenium-webdriver/findelement-and-findelements-command/ - Practice Exercise 1 */
		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		WebElement elementFirstName = driver.findElement(By.name("firstname"));
		elementFirstName.sendKeys("Jakab");
		WebElement elementLastName = driver.findElement(By.name("lastname"));
		elementLastName.sendKeys("Gipsz");
		driver.findElement(By.id("submit"));
		
		/* http://toolsqa.com/selenium-webdriver/findelement-and-findelements-command/ - Practice Exercise 2 
		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		driver.findElement(By.partialLinkText("Partial")).click();
		String buttonName =	driver.findElement(By.tagName("button")).toString();
		System.out.println(buttonName);
		driver.findElement(By.linkText("Link Test")); */
		
		/* http://toolsqa.com/selenium-webdriver/checkbox-radio-button-operations/ Practice Exercise / Challenge One
		driver.get("http://toolsqa.com/automation-practice-form/");
		List<WebElement> rdBtn_Sex = driver.findElements(By.name("sex"));
		if ( rdBtn_Sex.get(0).isSelected() == true ) {
			rdBtn_Sex.get(1).click();
		} else {
			rdBtn_Sex.get(0).click();
		} */
		
		/* http://toolsqa.com/selenium-webdriver/checkbox-radio-button-operations/ Practice Exercise / Challenge Two
		driver.get("http://toolsqa.com/automation-practice-form/");
		WebElement rdBtn_Year3 = driver.findElement(By.id("exp-2"));
		rdBtn_Year3.click(); */
		
		/* http://toolsqa.com/selenium-webdriver/checkbox-radio-button-operations/ Practice Exercise / Challenge Three
		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		List<WebElement> checkBox = driver.findElements(By.name("profession"));
		int iSize = checkBox.size();
		for (int i = 0; i < iSize; i++) {
			String checkBoxValue = checkBox.get(i).getAttribute("value");
			if (checkBoxValue.equalsIgnoreCase("Automation Tester")) {
				checkBox.get(i).click();
				break;
			}
		} */
		
		/* http://toolsqa.com/selenium-webdriver/checkbox-radio-button-operations/ Practice Exercise / Challenge Four
		driver.get("http://toolsqa.com/automation-practice-form/");
		WebElement checkBoxAutomationToolSIDE = driver.findElement(By.cssSelector("input[value='Selenium IDE'"));
		checkBoxAutomationToolSIDE.click(); */
	
	//driver.quit();
	}
}
